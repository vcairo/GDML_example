
#include <vector>

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4TransportationManager.hh"

#include "ActionInitialization.hh"
#include "G01DetectorConstruction.hh"
#include "FTFP_BERT.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include "G4GDMLParser.hh"

#include "HistoManager.hh"

void usage()
{
   G4cout << G4endl;
   G4cout << "Usage: load_gdml <opt1> <opt2> ..."<<G4endl;
   G4cout << "Options: "<<G4endl;
   G4cout << "-h , -help  : this help."<<G4endl;
   G4cout << "-n <nEvent> : nr. of events to be run in batch mode [10000]."<<G4endl;
   G4cout << "-f <file>   : input GDML file [InclBrl4.gdml]."<<G4endl;
   G4cout << "-o <file>   : output GDML file."<<G4endl;
   G4cout << "-v <iLevel> : verbosity level."<<G4endl;
   G4cout << "-i          : interactive mode."<<G4endl;
   
}

int main(int argc,char **argv)
{
   
   int nargs=0;
   int verbosity=0;
   std::string fileIn="InclBrl4.gdml";
   std::string fileOut="";
   bool interactive=false;
   int nEvents=10000;
   
   std::string histoFileOut="out.root";

   while (nargs<(argc-1))
   {
       nargs++;
	   std::string sarg=argv[nargs];
	   
	   if (sarg=="-help")
	   {
	      usage();
		  exit(1);
       }
	   else if (sarg=="-f")
	   {
	      nargs++;
		  fileIn=argv[nargs];
	   }
	   else if (sarg=="-n")
	   {
	   	  nargs++;
	  	  nEvents=std::atoi(argv[nargs]);
	   }
	   else if (sarg=="-v")
	   {
	   	  nargs++;
	  	  verbosity=std::atoi(argv[nargs]);
	   }
	   else if (sarg=="-o")
	   {
	   	  nargs++;
	  	  fileOut=argv[nargs];
	   }
	   else if (sarg=="-i")
	   {
	   	  interactive=true;
	   }
	   else if (sarg=="-h")
	   {
	   	  nargs++;
		  histoFileOut=argv[nargs];
	   }
	   else
	   {
	   	  std::cout<<" unknown option "<<argv[nargs]<<std::endl;
		  exit(10);
	   }
   }
   
   G4GDMLParser parser;
   
   HistoManager* hm=HistoManager::GetHistoManager(histoFileOut);
   hm->Book();

// Uncomment the following if wish to avoid names stripping
   // parser.SetStripFlag(false);

   parser.Read(fileIn.c_str(),false);
   

   G4RunManager* runManager = new G4RunManager;

   runManager->SetUserInitialization(new G01DetectorConstruction(
                                     parser.GetWorldVolume()));
   runManager->SetUserInitialization(new FTFP_BERT);
   
   runManager->SetUserInitialization(new ActionInitialization);

   runManager->Initialize();

   G4UImanager* UImanager = G4UImanager::GetUIpointer();
 
   ///////////////////////////////////////////////////////////////////////
   //
   // Example how to retrieve Auxiliary Information
   //

   std::cout << std::endl;
   
   
   //
   // End of Auxiliary Information block
   //
   ////////////////////////////////////////////////////////////////////////

   // example of writing out
   
   if (!fileOut.empty())
   {
     parser.Write(fileOut.c_str(), G4TransportationManager::GetTransportationManager()
      ->GetNavigatorForTracking()->GetWorldVolume()->GetLogicalVolume());
   }
   
   if (verbosity)
   {
   		std::ostringstream s;
		s<<"/tracking/verbose "<<verbosity;
		UImanager->ApplyCommand(s.str());
   }

   if (interactive)           // interactive mode
   {
#ifdef G4UI_USE
     G4UIExecutive* ui = new G4UIExecutive(argc, argv);
#ifdef G4VIS_USE
     G4VisManager* visManager = new G4VisExecutive;
     visManager->Initialize();
     UImanager->ApplyCommand("/control/execute vis.mac");
#endif
     ui->SessionStart();
#ifdef G4VIS_USE
     delete visManager;
#endif
     delete ui;
#endif
   }
   else
   {
   	 std::ostringstream s;
	 s<<"/run/beamOn "<<nEvents;
     UImanager->ApplyCommand(s.str());
   }

   hm->Save();
   delete runManager;

   return 0;
}

   
