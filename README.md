

This example demonstrates the usage of the GDML reader and writer. It allows
to export geometry descriptions in an application independent format (GDML,
Geometry Description Markup Language).
The GDML files can be then used to interchange geometries between different
applications and users.

The detector construction consists of a call to GDMLProcessor which parses a
GDML file and returns the pointer to the world volume. The user can also write
her/his own GDML file and use it as the primary input format for her/his Geant4
application.

Installation
------------

to set up Geant4 follow the instructions at 
http://lcgapp.cern.ch/project/simu/geant4/gettingStarted.html
the procedure does NOT work when running zsh though, it is best to run 
under bash:

> bash
> export G4HOME=/afs/cern.ch/sw/lcg/external/geant4/10.2.p02/x86_64-slc6-gcc49-opt-MT
> source /afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/setup.sh
> source $G4HOME/CMake-setup.sh
> export CXX=/afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/bin/g++
> export CC=/afs/cern.ch/sw/lcg/contrib/gcc/4.9/x86_64-slc6/bin/gcc

to compile the example create a build directory, cd into it and run cmake:

> mkdir build
> cmake -DGeant4_DIR=$G4HOME ..
> make

to run the example:

> ./load_gdml -h

Usage: load_gdml <opt1> <opt2> ...
Options: 
-h , -help  : this help.
-n <nEvent> : nr. of events to be run in batch mode [10000].
-f <file>   : input GDML file [InclBrl4.gdml].
-o <file>   : output GDML file.
-i          : interactive mode.

the main program is locate in the GDML_example directory (load_gdml.cc), other
header and source files are in the include and src directories. Every time a 
file is modified, one must go back to the build directory and "make". Every time
a source file is added or removed, the file CMakeCache.txt in the build directory
must be removed, otherwise the makefile will not take the changes into account!

More functionality to come in the next few days....
