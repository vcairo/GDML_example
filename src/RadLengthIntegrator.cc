
#include "RadLengthIntegrator.hh"
#include "SingleParticleGeneratorAction.hh"
#include "HistoManager.hh" 

#include "G4ios.hh"
#include "G4Timer.hh"
#include "G4Step.hh"
#include "G4Material.hh"
#include "G4StepPoint.hh"
#include "G4Event.hh"

#include <iostream>

RadLengthIntegrator::RadLengthIntegrator()
: G4UserSteppingAction(),G4UserEventAction()
{
	std::cout<<" RadLengthIntegrator being created "<<std::endl;
}


RadLengthIntegrator::~RadLengthIntegrator()
{
}

void RadLengthIntegrator::UserSteppingAction(const G4Step* step)
{
	//std::cout<<" This is RadLengthIntegrator "<<std::endl;
	
	double stepLength=step->GetStepLength();
	G4StepPoint* preStep=step->GetPreStepPoint();
	G4Material* material=preStep->GetMaterial();
	double radLen=material->GetRadlen();
	if (radLen>200000) return;
	//std::cout<<" This is RadLengthIntegrator material: "<<material->GetName()<<
	// " rad. length "<<material->GetRadlen()<<" step length "<<stepLength<<std::endl;
	double tck=100.*stepLength/radLen;
	std::string matName=material->GetName();
	if (radLengths.find(matName)!=radLengths.end())
		radLengths[matName]+=tck;
	else
		radLengths[matName]=tck;
} 

void RadLengthIntegrator::BeginOfEventAction(const G4Event*)
{
	radLengths.clear();
}
void RadLengthIntegrator::EndOfEventAction(const G4Event*)
{
	double eta,phi;
	eta=SingleParticleGeneratorAction::GetEtaValue();
	phi=SingleParticleGeneratorAction::GetEtaValue();
	
	HistoManager* hm=HistoManager::GetHistoManager("dummy");
	//std::cout<<std::endl;
	//std::cout<<" Material Ticknesses (in % of a rad.len. :"<<std::endl;
	
	double tot=0.;
	for (auto it: radLengths )
	{
		tot+=it.second;
		hm->BookAndFillProfile(it.first,it.first,100,-3.,3.,0,100.,eta,it.second);
		//  std::cout<<"\t "<<it.first<<"\t\t "<<it.second<<std::endl;
	}
	hm->BookAndFillProfile("total","total",100,-3.,3.,0,100.,eta,tot);
	//std::cout<<std::endl;
}

