#include "SingleParticleGeneratorMessenger.hh"
#include "SingleParticleGeneratorAction.hh"

#include "G4SystemOfUnits.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"

SingleParticleGeneratorMessenger::SingleParticleGeneratorMessenger(SingleParticleGeneratorAction *spg)
	:singleParticleGenerator(spg)
{
	generatorDirectory=new G4UIdirectory("/single_particle/");
	
	particleType = new G4UIcmdWithAString("/single+particle/particle",this);
	particleType->SetGuidance("Set particle to be generated.");
	particleType->SetGuidance(" (geantino is default)");
	particleType->SetParameterName("particleName",true);
	particleType->SetDefaultValue("geantino");
	
  	minEnergy = new G4UIcmdWithADoubleAndUnit("/single_particle/minEnergy",this);
 	minEnergy->SetGuidance("Set minimum kinetic energy.");
 	minEnergy->SetParameterName("MinEnergy",true,true);
 	minEnergy->SetDefaultUnit("GeV");
	
	maxEnergy = new G4UIcmdWithADoubleAndUnit("/single_particle/maxEnergy",this);
 	maxEnergy->SetGuidance("Set maximum kinetic energy.");
 	maxEnergy->SetParameterName("MaxEnergy",true,true);
 	maxEnergy->SetDefaultUnit("GeV");
	
	minPhi = new G4UIcmdWithADoubleAndUnit("/single_particle/minPhi",this);
 	minPhi->SetGuidance("Set minimum phi.");
 	minPhi->SetParameterName("MinPhi",true,true);
 	minPhi->SetDefaultUnit("deg");
	
	maxPhi = new G4UIcmdWithADoubleAndUnit("/single_particle/maxPhi",this);
 	maxPhi->SetGuidance("Set maximum phi.");
 	maxPhi->SetParameterName("MaxPhi",true,true);
 	maxPhi->SetDefaultUnit("deg");
	
	minEta = new G4UIcmdWithADouble("/single_particle/minEta",this);
 	minEta->SetGuidance("Set minimum eta.");
 	minEta->SetParameterName("MinEta",true,true);
	
	maxEta = new G4UIcmdWithADouble("/single_particle/maxEta",this);
 	maxEta->SetGuidance("Set maximum eta.");
 	maxEta->SetParameterName("MaxEta",true,true);
	
	singleParticleGenerator->SetParticleType("geantino");
	singleParticleGenerator->SetEnergyRange(1*GeV,1*TeV);
	singleParticleGenerator->SetEtaRange(-3.,3.);
	singleParticleGenerator->SetPhiRange();
	singleParticleGenerator->SetVertexPosition(0,0,0);

}

SingleParticleGeneratorMessenger::~SingleParticleGeneratorMessenger()
{
	delete generatorDirectory;
	delete particleType;
	delete minEnergy;
	delete maxEnergy;
	delete minEta;
	delete maxEta;
	delete minPhi;
	delete maxPhi;
}

void SingleParticleGeneratorMessenger::SetNewValue(G4UIcommand * command,G4String newValues)
{
	if (command==particleType)
	{
		singleParticleGenerator->SetParticleType(newValues);
	}
	else if (command==minEnergy)
	{
		singleParticleGenerator->SetEnergyMin(minEnergy->GetNewDoubleValue(newValues));
	}
	else if (command==maxEnergy)
	{
		singleParticleGenerator->SetEnergyMax(maxEnergy->GetNewDoubleValue(newValues));
	}
	else if (command==minEta)
	{
		singleParticleGenerator->SetEtaMin(minEta->GetNewDoubleValue(newValues));
	}
	else if (command==maxEta)
	{
		singleParticleGenerator->SetEtaMax(maxEta->GetNewDoubleValue(newValues));
	}
	else if (command==minPhi)
	{
		singleParticleGenerator->SetPhiMin(minPhi->GetNewDoubleValue(newValues));
	}
	else if (command==maxPhi)
	{
		singleParticleGenerator->SetPhiMax(maxPhi->GetNewDoubleValue(newValues));
	}
}

G4String SingleParticleGeneratorMessenger::GetCurrentValue(G4UIcommand * command)
{
	G4String str="not implemented";
	
		if (command==particleType)
	{
		str=singleParticleGenerator->GetParticleType();
	}
	else if (command==minEnergy)
	{
		str=minEnergy->ConvertToString(singleParticleGenerator->GetEnergyMin(),"GeV");
	}
	else if (command==maxEnergy)
	{
		str=maxEnergy->ConvertToString(singleParticleGenerator->GetEnergyMax(),"GeV");
	}
	else if (command==minEta)
	{
		str=minEta->ConvertToString(singleParticleGenerator->GetEtaMin());
	}
	else if (command==maxEta)
	{
		str=maxEta->ConvertToString(singleParticleGenerator->GetEtaMax());
	}
	else if (command==minPhi)
	{
		str=minPhi->ConvertToString(singleParticleGenerator->GetPhiMin(),"deg");
	}
	else if (command==maxPhi)
	{
		str=maxPhi->ConvertToString(singleParticleGenerator->GetPhiMax(),"deg");
	}
	
	return str;
}
