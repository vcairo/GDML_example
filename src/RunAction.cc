
#include "RunAction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4Timer.hh"


RunAction::RunAction()
 : G4UserRunAction()
{ 
  // set printing event number per each 100 events
  G4RunManager::GetRunManager()->SetPrintProgress(100);
  timer=new G4Timer;     
}

RunAction::~RunAction()
{
  delete timer;
}

void RunAction::BeginOfRunAction(const G4Run*)
{ 
  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);     
  std::cout<<" timer starts "<<std::endl;
  timer->Start();
}

void RunAction::EndOfRunAction(const G4Run* )
{
  timer->Stop();
  G4cout.precision(20);
  G4cout<<"total time for the run = "<<timer->GetUserElapsed()<<" s"<<G4endl;
}
