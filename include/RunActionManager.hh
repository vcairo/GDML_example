
#ifndef RunActionManager_h
#define RunActionManager_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;
class G4Timer;

#include<map>
#include<string>

/// Run action class

class RunActionManager : public G4UserRunAction
{
  public:
    RunActionManager();
    virtual ~RunActionManager();

    virtual void BeginOfRunAction(const G4Run* run);
    virtual void   EndOfRunAction(const G4Run* run);
	
	void SetRunAction(std::string, G4UserRunAction*);
  private:
	G4Timer* timer;
	
	int nInstances=0;
	
	std::map<std::string,G4UserRunAction*> runActionList;
};


#endif
