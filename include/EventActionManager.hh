
#ifndef EventActionManager_h
#define EventActionManager_h 1

#include "G4UserEventAction.hh"

#include "globals.hh"

#include <map>
#include <string>

/// Event action class

class G4Timer;

class EventActionManager : public G4UserEventAction
{
  public:
    EventActionManager();
    virtual ~EventActionManager();

    virtual void    BeginOfEventAction(const G4Event* );
    virtual void    EndOfEventAction(const G4Event* );
	
	void SetEventAction(std::string,G4UserEventAction* );
  private:
  
  	int nInstances=0;
  	
	std::map<std::string,G4UserEventAction*> eventActionList;
};

#endif
