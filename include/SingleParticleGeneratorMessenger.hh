#ifndef SingleParticleGeneratorMessenger_H
#define SingleParticleGeneratorMessenger_H

class SingleParticleGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;

#include "G4UImessenger.hh"
#include "globals.hh"

class SingleParticleGeneratorMessenger: public G4UImessenger{
public:
	SingleParticleGeneratorMessenger(SingleParticleGeneratorAction*);
	~SingleParticleGeneratorMessenger();
	
	void SetNewValue(G4UIcommand * command,G4String newValues);
	G4String GetCurrentValue(G4UIcommand * command);
	
private:
	SingleParticleGeneratorAction* singleParticleGenerator;
	
	G4UIdirectory *             generatorDirectory;
	G4UIcmdWithAString*			particleType;
	G4UIcmdWithADoubleAndUnit*  minEnergy;
	G4UIcmdWithADoubleAndUnit*  maxEnergy;
	G4UIcmdWithADouble*			minEta;
	G4UIcmdWithADouble*			maxEta;
	G4UIcmdWithADoubleAndUnit*  minPhi;
	G4UIcmdWithADoubleAndUnit*  maxPhi;
};

#endif
