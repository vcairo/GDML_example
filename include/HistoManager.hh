
#ifndef HistoManager_h
#define HistoManager_h 1

#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class TFile;
class TTree;
class TH1D;
class TH1;
class TH2D;
class TProfile;

#include <map>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class HistoManager
{
  private:
    HistoManager();
    HistoManager(std::string);
   ~HistoManager();
  public:
    void Book();
    void Save();
	
	static HistoManager* GetHistoManager(std::string);

// TH1D	
	TH1D* Book1DHistogram(std::string, std::string, int, double, double);
	void BookAndFill1D(std::string name, std::string type, int nchx, double xmin, double xmax, double val, double w=1.);
// TProfile
	TProfile* BookProfileHistogram(std::string, std::string, int, double, double, double, double);
	void BookAndFillProfile(std::string name, std::string type, int nchx, double xmin, double xmax, double ymin, double ymax, double valx, double valy, double w=1.);
// TH2D
	TH2D* Book2DHistogram(std::string, std::string, int, double, double,int, double, double);
    void BookAndFill2D(std::string, std::string, int, double xmin, double xmax,int nchy, double ymin, double ymax, double xval, double yval, double w=1.);

    template <typename T> T* getHisto(std::string name) 
	{
		return dynamic_cast<T*>(histogramMap[name]);
	}
    
    void setFileName(std::string s) {fileName=s;}
        
  private:
    TFile*   fRootFile;
    std::string fileName;
	
	std::map<std::string, TH1*> histogramMap;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

